# Subject 1
# TOPIC: HTML, JS, JSON

# Having the following application developed in NodeJS with static content served from the `public` directory, complete the tasks.

# TASK 1: Modify `index.html` file to complete the following requirements (0.8 pts)
- Add a heading element; (0.2 pts)
- Heading element should contain `Welcome to Homepage` text; (0.2 pts)
- Add an image from `assets` directory; (0.2)
- Add 2 anchor elements with the following ids: `home` and `register` and text: `Home` and `Register`. The anchors should redirect you to homepage and `register.html`; (0.2 pts)

# TASK 2: Modify `register.html` file to complete the following requirements (0.6 pts)
- Add a heading element with text `Register Now`; (0.2 pts)
- Add a form element that contains 4 inputs: Full Name, Email, Password and Complete Password; (0.2 pts)
- Add a submit button with the id `submit-btn` and text `Register`; (0.2 pts)

# TASK 3: Modify function `displayJSON(fullName, email, password)` found in `js/script.js` file to complete the following requirements (0.6 pts)
- Function should return a JSON with properties `fullName, email and password`; (0.2 pts)
- If no value is passed for a parameter, the JSON returned should contained `DEFAULT_VALUE` for that property; (0.2 pts)
- Function should throw an error if `fullName` includes text: `admin`; (0.2 pts)